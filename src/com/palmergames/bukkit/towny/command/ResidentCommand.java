package com.palmergames.bukkit.towny.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.TownyFormatter;
import com.palmergames.bukkit.towny.TownyMessaging;
import com.palmergames.bukkit.towny.TownySettings;
import com.palmergames.bukkit.towny.exceptions.AlreadyRegisteredException;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.exceptions.TownyException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.TownyPermission;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.palmergames.bukkit.towny.permissions.PermissionNodes;
import com.palmergames.bukkit.util.ChatTools;
import com.palmergames.bukkit.util.Colors;
import com.palmergames.util.StringMgmt;

/**
 * Send a list of all towny resident help commands to player Command: /resident
 */

public class ResidentCommand implements CommandExecutor {

	private static Towny plugin;
	private static final List<String> output = new ArrayList<String>();

	static {
		output.add(ChatTools.formatTitle("/주민"));
		output.add(ChatTools.formatCommand("", "/주민", "", TownySettings.getLangString("res_1")));
		output.add(ChatTools.formatCommand("", "/주민", TownySettings.getLangString("res_2"), TownySettings.getLangString("res_3")));
		output.add(ChatTools.formatCommand("", "/주민", "목록", TownySettings.getLangString("res_4")));
		output.add(ChatTools.formatCommand("", "/주민", "세금", ""));
		output.add(ChatTools.formatCommand("", "/주민", "토글", "[모드]...[모드]"));
		output.add(ChatTools.formatCommand("", "/주민", "설정 [] .. []", "'/주민 설정' " + TownySettings.getLangString("res_5")));
		output.add(ChatTools.formatCommand("", "/주민", "친구 [추가/제거] " + TownySettings.getLangString("res_2"), TownySettings.getLangString("res_6")));
		output.add(ChatTools.formatCommand("", "/주민", "친구 [추가+/제거+] " + TownySettings.getLangString("res_2") + " ", TownySettings.getLangString("res_7")));
		//output.add(ChatTools.formatCommand(TownySettings.getLangString("admin_sing"), "/resident", "delete " + TownySettings.getLangString("res_2"), ""));
	}

	public ResidentCommand(Towny instance) {

		plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if (sender instanceof Player) {
			Player player = (Player) sender;
			System.out.println("[PLAYER_COMMAND] " + player.getName() + ": /" + commandLabel + " " + StringMgmt.join(args));
			if (args == null) {
				for (String line : output)
					player.sendMessage(line);
				parseResidentCommand(player, args);
			} else {
				parseResidentCommand(player, args);
			}

		} else
			// Console
			for (String line : output)
				sender.sendMessage(Colors.strip(line));
		return true;
	}

	public void parseResidentCommand(Player player, String[] split) {

		try {

			if (split.length == 0) {

				try {
					Resident resident = TownyUniverse.getDataSource().getResident(player.getName());
					TownyMessaging.sendMessage(player, TownyFormatter.getStatus(resident, player));
				} catch (NotRegisteredException x) {
					throw new TownyException(TownySettings.getLangString("msg_err_not_registered"));
				}

			} else if (split[0].equalsIgnoreCase("?") || split[0].equalsIgnoreCase("도움말")) {

				for (String line : output)
					player.sendMessage(line);

			} else if (split[0].equalsIgnoreCase("목록")) {

				if (!TownyUniverse.getPermissionSource().testPermission(player, PermissionNodes.TOWNY_COMMAND_RESIDENT_LIST.getNode()))
					throw new TownyException(TownySettings.getLangString("msg_err_command_disable"));

				listResidents(player);

			} else if (split[0].equalsIgnoreCase("세금")) {

				if (!TownyUniverse.getPermissionSource().testPermission(player, PermissionNodes.TOWNY_COMMAND_RESIDENT_TAX.getNode()))
					throw new TownyException(TownySettings.getLangString("msg_err_command_disable"));

				try {
					Resident resident = TownyUniverse.getDataSource().getResident(player.getName());
					TownyMessaging.sendMessage(player, TownyFormatter.getTaxStatus(resident));
				} catch (NotRegisteredException x) {
					throw new TownyException(TownySettings.getLangString("msg_err_not_registered"));
				}

			} else if (split[0].equalsIgnoreCase("설정")) {

				/*
				 * perms checked in method.
				 */
				String[] newSplit = StringMgmt.remFirstArg(split);
				residentSet(player, newSplit);

			} else if (split[0].equalsIgnoreCase("토글")) {

				/*
				 * 
				 */
				String[] newSplit = StringMgmt.remFirstArg(split);
				residentToggle(player, newSplit);

			} else if (split[0].equalsIgnoreCase("친구")) {

				if (!TownyUniverse.getPermissionSource().testPermission(player, PermissionNodes.TOWNY_COMMAND_RESIDENT_FRIEND.getNode()))
					throw new TownyException(TownySettings.getLangString("msg_err_command_disable"));

				String[] newSplit = StringMgmt.remFirstArg(split);
				residentFriend(player, newSplit);

			} else {

				try {
					Resident resident = TownyUniverse.getDataSource().getResident(split[0]);
					TownyMessaging.sendMessage(player, TownyFormatter.getStatus(resident, player));
				} catch (NotRegisteredException x) {
					throw new TownyException(String.format(TownySettings.getLangString("msg_err_not_registered_1"), split[0]));
				}
			}

		} catch (Exception x) {
			TownyMessaging.sendErrorMsg(player, x.getMessage());
		}
	}

	/**
	 * Toggle modes for this player.
	 * 
	 * @param player
	 * @param newSplit
	 * @throws TownyException
	 */
	private void residentToggle(Player player, String[] newSplit) throws TownyException {

		if (!TownyUniverse.getPermissionSource().testPermission(player, PermissionNodes.TOWNY_COMMAND_RESIDENT_TOGGLE.getNode(newSplit[0].toLowerCase()))
				|| newSplit[0].equalsIgnoreCase("spy"))
			throw new TownyException(TownySettings.getLangString("msg_err_command_disable"));

		try {
			Resident resident = TownyUniverse.getDataSource().getResident(player.getName());

			TownyPermission perm = resident.getPermissions();

			if (newSplit[0].equalsIgnoreCase("pvp")) {
				perm.pvp = !perm.pvp;
			} else if (newSplit[0].equalsIgnoreCase("불")) {
				perm.fire = !perm.fire;
			} else if (newSplit[0].equalsIgnoreCase("폭발")) {
				perm.explosion = !perm.explosion;
			} else if (newSplit[0].equalsIgnoreCase("몹")) {
				perm.mobs = !perm.mobs;
			} else {

				resident.toggleMode(newSplit, true);
				return;
			}

			notifyPerms(player, perm);
			TownyUniverse.getDataSource().saveResident(resident);

		} catch (NotRegisteredException e) {
			// unknown resident
			throw new TownyException(String.format(TownySettings.getLangString("msg_err_not_registered"), player.getName()));
		}

	}

	/**
	 * Show the player the new Permission settings after the toggle.
	 * 
	 * @param player
	 * @param perm
	 */
	private void notifyPerms(Player player, TownyPermission perm) {

		TownyMessaging.sendMsg(player, TownySettings.getLangString("msg_set_perms"));
		TownyMessaging.sendMessage(player, Colors.Green + "PvP: " + ((perm.pvp) ? Colors.Red + "켜짐" : Colors.LightGreen + "꺼짐") + Colors.Green + "  폭발: " + ((perm.explosion) ? Colors.Red + "켜짐" : Colors.LightGreen + "꺼짐") + Colors.Green + "  불번짐: " + ((perm.fire) ? Colors.Red + "켜짐" : Colors.LightGreen + "꺼짐") + Colors.Green + "  몹 스폰: " + ((perm.mobs) ? Colors.Red + "켜짐" : Colors.LightGreen + "꺼짐"));

	}

	public void listResidents(Player player) {

		player.sendMessage(ChatTools.formatTitle(TownySettings.getLangString("res_list")));
		String colour;
		ArrayList<String> formatedList = new ArrayList<String>();
		for (Resident resident : plugin.getTownyUniverse().getActiveResidents()) {
			if (resident.isKing())
				colour = Colors.Gold;
			else if (resident.isMayor())
				colour = Colors.LightBlue;
			else
				colour = Colors.White;
			formatedList.add(colour + resident.getName() + Colors.White);
		}
		for (String line : ChatTools.list(formatedList))
			player.sendMessage(line);
	}

	/**
	 * 
	 * Command: /resident set [] ... []
	 * 
	 * @param player
	 * @param split
	 * @throws TownyException
	 */

	/*
	 * perm [resident/outsider] [build/destroy] [on/off]
	 */

	public void residentSet(Player player, String[] split) throws TownyException {

		if (split.length == 0) {
			player.sendMessage(ChatTools.formatCommand("", "/주민 설정", "권한 ...", "'/주민 설정 권한' " + TownySettings.getLangString("res_5")));
			player.sendMessage(ChatTools.formatCommand("", "/주민 설정", "모드 ...", "'/주민 설정 모드' " + TownySettings.getLangString("res_5")));
		} else {
			Resident resident;
			try {
				resident = TownyUniverse.getDataSource().getResident(player.getName());
			} catch (TownyException x) {
				TownyMessaging.sendErrorMsg(player, x.getMessage());
				return;
			}

			if (!TownyUniverse.getPermissionSource().testPermission(player, PermissionNodes.TOWNY_COMMAND_RESIDENT_SET.getNode(split[0].toLowerCase())))
				throw new TownyException(TownySettings.getLangString("msg_err_command_disable"));

			// TODO: Let admin's call a subfunction of this.
			if (split[0].equalsIgnoreCase("권한")) {
				String[] newSplit = StringMgmt.remFirstArg(split);
				TownCommand.setTownBlockPermissions(player, resident, resident.getPermissions(), newSplit, true);
			} else if (split[0].equalsIgnoreCase("모드")) {
				String[] newSplit = StringMgmt.remFirstArg(split);
				setMode(player, newSplit);
			} else {
				TownyMessaging.sendErrorMsg(player, String.format(TownySettings.getLangString("msg_err_invalid_property"), "town"));
				return;
			}

			TownyUniverse.getDataSource().saveResident(resident);
		}
	}

	private void setMode(Player player, String[] split) {

		if (split.length == 0) {
			player.sendMessage(ChatTools.formatCommand("", "/주민 설정 모드", "청소", ""));
			player.sendMessage(ChatTools.formatCommand("", "/주민 설정 모드", "[모드] ...[모드]", ""));
			player.sendMessage(ChatTools.formatCommand("모드", "지도", "", TownySettings.getLangString("mode_1")));
			player.sendMessage(ChatTools.formatCommand("모드", "마을영토", "", TownySettings.getLangString("mode_2")));
			player.sendMessage(ChatTools.formatCommand("모드", "마을영토해제", "", TownySettings.getLangString("mode_3")));
			player.sendMessage(ChatTools.formatCommand("모드", "tc", "", TownySettings.getLangString("mode_4")));
			player.sendMessage(ChatTools.formatCommand("모드", "nc", "", TownySettings.getLangString("mode_5")));
			// String warFlagMaterial = (TownyWarConfig.getFlagBaseMaterial() ==
			// null ? "flag" :
			// TownyWarConfig.getFlagBaseMaterial().name().toLowerCase());
			// player.sendMessage(ChatTools.formatCommand("Mode", "warflag", "",
			// String.format(TownySettings.getLangString("mode_6"),
			// warFlagMaterial)));
			player.sendMessage(ChatTools.formatCommand("예제", "/주민 설정 모드", "기본적인 국가,마을,지도", ""));

			return;
		}

		if (split[0].equalsIgnoreCase("reset") || split[0].equalsIgnoreCase("재설정")) {
			plugin.removePlayerMode(player);
			return;
		}

		List<String> list = Arrays.asList(split);
		if ((list.contains("spy")) && (plugin.isPermissions() && !TownyUniverse.getPermissionSource().has(player, PermissionNodes.TOWNY_CHAT_SPY.getNode()))) {
			TownyMessaging.sendErrorMsg(player, TownySettings.getLangString("msg_err_command_disable"));
			return;
		}

		plugin.setPlayerMode(player, split, true);

	}

	public void residentFriend(Player player, String[] split) {

		if (split.length == 0) {
			player.sendMessage(ChatTools.formatCommand("", "/주민 친구", "추가 " + TownySettings.getLangString("res_2"), ""));
			player.sendMessage(ChatTools.formatCommand("", "/주민 친구", "제거 " + TownySettings.getLangString("res_2"), ""));
			player.sendMessage(ChatTools.formatCommand("", "/주민 친구", "청소", ""));
		} else {
			Resident resident;
			try {
				resident = TownyUniverse.getDataSource().getResident(player.getName());
			} catch (TownyException x) {
				TownyMessaging.sendErrorMsg(player, x.getMessage());
				return;
			}

			// TODO: Let admin's call a subfunction of this.
			if (split[0].equalsIgnoreCase("추가")) {
				String[] names = StringMgmt.remFirstArg(split);
				residentFriendAdd(player, resident, TownyUniverse.getOnlineResidents(player, names));
			} else if (split[0].equalsIgnoreCase("제거")) {
				String[] names = StringMgmt.remFirstArg(split);
				residentFriendRemove(player, resident, TownyUniverse.getOnlineResidents(player, names));
			} else if (split[0].equalsIgnoreCase("추가+")) {
				String[] names = StringMgmt.remFirstArg(split);
				residentFriendAdd(player, resident, getResidents(player, names));
			} else if (split[0].equalsIgnoreCase("제거+")) {
				String[] names = StringMgmt.remFirstArg(split);
				residentFriendRemove(player, resident, getResidents(player, names));
			} else if (split[0].equalsIgnoreCase("목록청소") || split[0].equalsIgnoreCase("청소")) {
				residentFriendRemove(player, resident, resident.getFriends());
			}

		}
	}

	private static List<Resident> getResidents(Player player, String[] names) {

		List<Resident> invited = new ArrayList<Resident>();
		for (String name : names)
			try {
				Resident target = TownyUniverse.getDataSource().getResident(name);
				invited.add(target);
			} catch (TownyException x) {
				TownyMessaging.sendErrorMsg(player, x.getMessage());
			}
		return invited;
	}

	public void residentFriendAdd(Player player, Resident resident, List<Resident> invited) {

		ArrayList<Resident> remove = new ArrayList<Resident>();
		for (Resident newFriend : invited)
			try {
				resident.addFriend(newFriend);
				plugin.deleteCache(newFriend.getName());
			} catch (AlreadyRegisteredException e) {
				remove.add(newFriend);
			}
		for (Resident newFriend : remove)
			invited.remove(newFriend);

		if (invited.size() > 0) {
			String msg = "Added ";
			for (Resident newFriend : invited) {
				msg += newFriend.getName() + ", ";
				Player p = plugin.getServer().getPlayer(newFriend.getName());
				if (p != null)
					TownyMessaging.sendMsg(p, String.format(TownySettings.getLangString("msg_friend_add"), player.getName()));
			}
			msg = msg.substring(0, msg.length() - 2);
			msg += TownySettings.getLangString("msg_to_list");
			TownyMessaging.sendMsg(player, msg);
			TownyUniverse.getDataSource().saveResident(resident);
		} else
			TownyMessaging.sendErrorMsg(player, TownySettings.getLangString("msg_invalid_name"));
	}

	public void residentFriendRemove(Player player, Resident resident, List<Resident> kicking) {

		List<Resident> remove = new ArrayList<Resident>();
		List<Resident> toKick = new ArrayList<Resident>(kicking);

		for (Resident friend : toKick) {
			try {
				resident.removeFriend(friend);
				plugin.deleteCache(friend.getName());
			} catch (NotRegisteredException e) {
				remove.add(friend);
			}
		}
		// remove invalid names so we don't try to send them messages
		if (remove.size() > 0)
			for (Resident friend : remove)
				toKick.remove(friend);

		if (toKick.size() > 0) {
			String msg = TownySettings.getLangString("msg_removed");
			Player p;
			for (Resident member : toKick) {
				msg += member.getName() + ", ";
				p = plugin.getServer().getPlayer(member.getName());
				if (p != null)
					TownyMessaging.sendMsg(p, String.format(TownySettings.getLangString("msg_friend_remove"), player.getName()));
			}
			msg = msg.substring(0, msg.length() - 2);
			msg += TownySettings.getLangString("msg_from_list");
			;
			TownyMessaging.sendMsg(player, msg);
			TownyUniverse.getDataSource().saveResident(resident);
		} else
			TownyMessaging.sendErrorMsg(player, TownySettings.getLangString("msg_invalid_name"));

	}

}
