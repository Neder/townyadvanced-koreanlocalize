Towny advanced is developed by ElgarL.

Releases, Dev builds and all other plugins I maintain/dev are available from...

* http://palmergames.com/towny

If you need help or just want to chat, join us in our IRC channel #towny on the Esper.net network.

I took over a few years ago and am just one of many devs who have held this position in the past.

-컴파일 시 안내-

윈도우용으로 컴파일 하실 분은 com.palmergames.util의 FileMgmt.java 에서
143라인, 180라인, 230라인 코드를 주석 해제해주시고, 그 아래의 코드를 주석처리 해주세요.
그렇게 컴파일해야 config 로드에서 에러가 발생하지 않습니다.
그 다음 소스에서 korean.yml을 빼신 다음 문자셋을 ANSI로 변경해주시고
아까 컴파일된 jar에 덮어씌워주세요.
그래야 에러가 발생하지 않습니다.

플러그인 컴파일 시 이클립스의 내보내기로 컴파일 하지 마시고 Ctrl + B 로 컴파일하세요.
필요한 라이브러리를 자동으로 다운받아 컴파일 해줍니다.
(혹시 다운이 안되신다면 bulid.xml을 열어 안에 있는 다운로드 URL을 한땀한땀 복사해서 다운받아 /lib에 투척하고 빌드경로 설정하세요...쩝)

한글화에 도움을 주신 분 : wolfwork (wolfdate25@gmail.com), 6four